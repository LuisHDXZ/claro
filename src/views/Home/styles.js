import { makeStyles } from '@mui/styles';
import { Images } from '../../assets';

const useStyles = makeStyles((theme) => ({
  counter: {
    padding: '4vh',
    height: '100vh',
    backgroundImage: `url(${Images.Background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    // marginTop: '2vh',
    backgroundColor: 'red',
    paddingTop: '14vh',
  }
}));

export { useStyles };
