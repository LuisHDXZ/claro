import React, { useState } from 'react'
import { Header } from '../../components/Header'
import { Counter } from '../../components/Counter'
import { CardVideo } from '../../components/CardVideo'
import { Footer } from '../../components/Footer'
import { Images } from '../../assets'
import { Grid } from '@mui/material'
import { useStyles } from './styles';
import ReactPlayer from 'react-player'


export const Home = () => {
  const classes = useStyles();
  const [videoActive, setvideoActive] = useState('https://www.youtube.com/watch?v=WcCC6fMER9g')

  const videos = [{
    title: 'TEAM LATAM',
    description: 'Los deportistas latinoamericanos que participan en sochi 2014.',
    image: Images.Card1,
    url: 'https://www.youtube.com/watch?v=IQPhLCkUugw'
  },
  {
    title: 'TRAVEL MEETS FASHION',
    description: 'Los deportistas latinoamericanos que participan en sochi 2014.',
    image: Images.Card2,
    url: 'https://www.youtube.com/watch?v=WcCC6fMER9g'
  },
  {
    title: 'VELOCIDAD Y RIESGO',
    description: 'Los deportistas latinoamericanos que participan en sochi 2014.',
    image: Images.Card3,
    url: 'https://www.youtube.com/watch?v=ixLPADNTgBI'
  },
  {
    title: 'EXPERIENCIA MULTIMEDIA',
    description: 'Los deportistas latinoamericanos que participan en sochi 2014.',
    image: Images.Card4,
    url: 'https://www.youtube.com/watch?v=Pd8W9WtMXDw'
  }]
  return (
    <Grid>
      <Header />
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        className={classes.counter}
      >
        <Grid item md={12}>
          <Counter />
        </Grid>
        <Grid style={{ height: '45vh', paddingTop: '1vh' }} md={6}>
          <ReactPlayer
            url={videoActive}
            className='react-player'
            playing
            width='100%'
            height='100%'
          />
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={4}
        style={{ backgroundColor: '#333333', padding: '3vh 0vh' }}
      >
        {videos.map(itemVideo =>
          <Grid item md={2}>
            <CardVideo video={itemVideo} clickVideo={() => setvideoActive(itemVideo.url)} />
          </Grid>
        )}
      </Grid>
      <Footer />
    </Grid>

  )
}
