import Card1 from './Images/team-latam.png'
import Card2 from './Images/travel-meets-fashion.png'
import Card3 from './Images/velocidad-riesgo.png'
import Card4 from './Images/experiencia.png'
import Background from './Images/background.jpg'
import Sochi from './Images/logo-sochi-color.png'
import ClaroLogo from './Images/logo-claro-sports.png'
import IconRed from './Images/mobli-icon.png'


export const Images = {
  Card1,
  Card2,
  Card3,
  Card4,
  Background,
  Sochi,
  ClaroLogo,
  IconRed
}