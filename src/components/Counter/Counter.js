import { Grid, Typography } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import { useStyles } from './styles';


export const Counter = () => {
  const classes = useStyles();
  const [timerDays, setTimerDays] = useState(0)
  const [timerHours, settimerHours] = useState(0)
  const [timerMinutes, settimerMinutes] = useState(0)
  const [timerSeconds, settimerSeconds] = useState(0)
  let interval = useRef()

  const countDown = () => {
    const countDownDate = new Date('Dec 31, 2022 00:00:00').getTime()
    interval = setInterval(() => {
      const now = new Date().getTime()
      const distance = countDownDate - now

      const days = Math.floor(distance / (1000 * 60 * 60 * 24))
      const hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60))
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      const seconds = Math.floor((distance % (1000 * 60)) / 1000)

      if (distance < 0) {
        clearInterval(interval.current)
      } else {
        setTimerDays(days)
        settimerHours(hours)
        settimerMinutes(minutes)
        settimerSeconds(seconds)
      }
    }, 1000);
  }

  useEffect(() => {
    countDown()
  }, [])

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
    >
      <Grid md={12}>
        <Typography className={classes.title}>FALTAN</Typography>
      </Grid>
      <Grid container
        direction="row"
        justifyContent="center"
        alignItems="center">
        <Grid sx={{ p: 2, m: 2 }} className={classes.circle} >
          <Typography className={classes.number}>{timerDays}</Typography>
          <Typography className={classes.day}>DÍAS</Typography>
        </Grid>
        <Grid sx={{ p: 2, m: 2 }} className={classes.circle}>
          <Typography className={classes.number}>{timerHours}</Typography>
          <Typography className={classes.day}>HRS</Typography>
        </Grid>
        <Grid sx={{ p: 2, m: 2 }} className={classes.circle}>
          <Typography className={classes.number}>{timerMinutes}</Typography>
          <Typography className={classes.day}>MIN</Typography>
        </Grid>
        <Grid sx={{ p: 2, m: 2 }} className={classes.circle}>
          <Typography className={classes.number}>{timerSeconds}</Typography>
          <Typography className={classes.day}>SEG</Typography>
        </Grid>
      </Grid>
    </Grid>
  )
}
