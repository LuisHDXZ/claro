import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  circle: {
    color: 'white',
    border: '2px solid white',
    borderRadius: '50%',
    textAlign: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    height: '10vh',
    width: '10vh',
  },
  number: {
    fontSize: '26px !important',
    lineHeight: '4vh !important',
    fontWeight: '800 !important'

  },
  title: {
    color: 'white',
    fontSize: '44px !important',
    textAlign: 'center',
    fontWeight: '800 !important'
  },
  day: {
    lineHeight: '2vh!important',
    fontWeight: '800 !important',
    fontFamily: 'Exo !important'
  }
}));

export { useStyles };
