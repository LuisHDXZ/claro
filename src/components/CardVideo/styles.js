import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  image: {
    cursor: 'pointer'
  },
  content_img: {
    position: 'relative',
    width: '200px',
    height: '200px',
    float: 'left',
    marginRight: '10px',
    '&:hover div': {
      // padding: '8px 15px',
      visibility: 'visible',
      opacity: '1',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
    }
  },
  contenText: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    background: '#FF5733',
    color: 'white',
    opacity: 0,
    visibility: 'hidden',
    '-webkit-transition': 'visibility 0s, opacity 0.5s linear',
    transition: 'visibility 0s, opacity 0.5s linear',
    width: '100%',
    height: '100%',
    borderRadius: '50%',
    cursor: 'pointer',
    fontWeight: 800,
    fontSize: '15px'
  }
}));

export { useStyles };
