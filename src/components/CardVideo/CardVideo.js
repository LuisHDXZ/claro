import { Grid, Typography } from '@mui/material'
import React from 'react'
import { useStyles } from './styles';


export const CardVideo = ({ video, clickVideo }) => {
  const classes = useStyles();

  return (
    <Grid onClick={() => clickVideo()}>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <div className={classes.content_img} >
          <img src={video.image} width='100%' height='100%' alt={video.title} />
          <div className={classes.contenText}>{video.description}</div>
        </div>

        <Typography style={{ color: 'white', cursor: 'pointer', fontWeight: 800 }}>{video.title}</Typography>
      </Grid>
    </Grid >

  )
}
