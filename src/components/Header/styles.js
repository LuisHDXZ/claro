import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: 'black',
    padding: '1vh',
    position: 'fixed',
  }
}));

export { useStyles };
