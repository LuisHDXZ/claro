import { Grid } from '@mui/material'
import React from 'react'
import { Images } from '../../assets'
import { useStyles } from './styles';

export const Header = () => {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="row"
      justifyContent="space-around"
      alignItems="center"
      className={classes.header}
    >
      <Grid item >
        <img src={Images.Sochi} alt='sochi' />
      </Grid>
      <Grid item >
        <img src={Images.ClaroLogo} alt='logoClaro' />
      </Grid>
    </Grid>
  )
}
