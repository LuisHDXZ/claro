import { Grid, Typography } from '@mui/material';
import React from 'react'
import { Images } from '../../assets';
import { useStyles } from './styles';

export const Footer = () => {
  const classes = useStyles();
  return (
    <Grid
      container
      direction="row"
      justifyContent="space-around"
      alignItems="center"
      className={classes.footer}
    >
      <Grid>
        <Typography className={classes.text}>Aviso de privacidad / Contacto</Typography>
      </Grid>
      <Grid>
        <img src={Images.Sochi} alt='logoSochi' />
      </Grid>
      <Grid item>
        <Grid
          container
          direction="row"
          justifyContent="space-around"
          alignItems="center">
          <Grid sx={{ ml: 1 }} className={classes.icons1}><img src={Images.IconRed} alt='icon' /></Grid>
          <Grid sx={{ ml: 1 }} className={classes.icons2}><img src={Images.IconRed} alt='icon' /></Grid>
          <Grid sx={{ ml: 1 }} className={classes.icons3}><img src={Images.IconRed} alt='icon' /></Grid>
          <Grid sx={{ ml: 1 }} className={classes.icons4}><img src={Images.IconRed} alt='icon' /></Grid>

        </Grid>
      </Grid>
    </Grid>
  )
}
