import { makeStyles } from '@mui/styles';

const stylesIcon = {
  padding: '3px',
  borderRadius: '10px',
  cursor: 'pointer'
}
const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: 'black'
  },
  text: {
    color: 'white',
    cursor: 'pointer',
    '&:hover': {
      color: 'blue'
    }
  },
  icons1: {
    backgroundColor: '#6699ff',
    ...stylesIcon
  },
  icons2: {
    backgroundColor: '#6699ff',
    ...stylesIcon
  },
  icons3: {
    backgroundColor: '#336699',
    ...stylesIcon
  }, icons4: {
    backgroundColor: '#993300',
    ...stylesIcon
  }
}));

export { useStyles };
